import Controller from '@ember/controller';
import { computed, observer, get } from '@ember/object';
import { match, not, gte, and } from '@ember/object/computed';
import { later } from '@ember/runloop';

export default Controller.extend({
  emailAddress: '',
  message: '',
  responseMessage: '',

  isValid: match('emailAddress', /^.+@.+\..+$/),
  isLongEnough: gte('message.length', 5),
  isDisabled: not('isBothTrue'),
  isBothTrue: and('isLongEnough', 'isValid'),

  actions: {
    sendMessage(){
      alert(`Saving the following is in progress, email: ${this.get('emailAddress')} message: ${this.get('message')}`);
      this.set('responseMessage', `We got your message and we’ll get in touch soon`);
      this.set('emailAddress', '');
      this.set('message', '');

      later(this, function() {
        this.set('responseMessage', '');
      }, 5000);
    }
  }

});
