import Controller from '@ember/controller';
import { computed, observer, get } from '@ember/object';
import { match, not } from '@ember/object/computed';

export default Controller.extend({
  emailAddress: '',
  headerMessage: 'Comming Soon',

  isValid: match('emailAddress', /^.+@.+\..+$/),
  isDisabled: not('isValid'),

  actions: {
    saveInvitation(){
      const email = this.get('emailAddress');
      const newInvitation = this.store.createRecord('invitation', {email});

      newInvitation.save().then(response => {
        this.set('responseMessage', `Thank you! We saved your email address with the following id: ${response.get('id')}`);
        this.set('emailAddress', '');
      });
    }
  }

});
